#include <sys/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
 #include <errno.h>


int main(int argc, const char** argv)
{
	if (argc < 2) {
		printf("Usage outb_flood port value [delay]\n");
		exit (1);
	}

	unsigned long port = strtoul(argv[1], NULL, 0);
	unsigned long value = strtoul(argv[2], NULL, 0);
	useconds_t delay = 0;
	int count = -1;

	if (argc > 3) {
		delay = strtoul(argv[3], NULL, 0);
	}

	if (argc > 4) {
		count = strtoul(argv[4], NULL, 0);
	}

	if (port > 0xFFFF || value > 0xFF) {
		printf("Error: port or value out of range\n");
		exit (1);

	}

	printf("port: 0x%x value: 0x%x delay: %d usec count: %d\n", port, value, delay, count);

	if (iopl(3)) {
		perror(NULL);
		exit (1);
	}

	while(1) {
		outb(value, port);
		if (delay)
			usleep(delay);

		if (count != -1) {
			if (!--count)
				break;
		}
	}
}
